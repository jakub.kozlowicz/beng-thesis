\chapter{Analiza problemu}

\section{Systemy wbudowane}

System wbudowany jest to połączenie oprogramowania oraz sprzętu komputerowego
zamknięte w większym urządzeniu. Systemy takie składają się z mikrokontrolera
lub mikroprocesora oraz urządzeń peryferyjnych ściśle dobranych do
zastosowania. W wielu przypadkach systemy takie są całkowicie odizolowane od
interakcji z użytkownikiem zewnętrznym~\cite{Hintermann:Embedded:Linux}. Można
je odnaleźć w szeregu urządzeń konsumenckich, takich jak inteligentne lodówki,
pralki czy bankomaty. W literaturze można znaleźć następujące sformułowanie
,,podstawą zdefiniowania systemu wbudowanego jest funkcja, jaką on pełni, oraz
miejsce jego zainstalowania''~\cite{Hayduk:Wbudowane:Mikrokomputery}.
Spowodowane jest to różnorodnością zastosowań takich systemów, zaczynając od
systemów sterujących programem w pralce, która ma przetrwać kilka lat, po
systemy sterowania lotem, które z założenia mają spełniać swoje zadania przez
dekady~\cite{Hayduk:Wbudowane:Mikrokomputery}. Systemy wbudowane cechuje
niezawodność, dokładność wskazywania informacji oraz energooszczędność. Wynika
to z faktu pracy w zamkniętym środowisku, gdzie naprawy są utrudnione lub
powodowałyby problemy organizacyjne~\cite{Buttazzo:Embedded:Trends}.

\vspace*{1em}

Urządzenia IoT są pewną częścią wszystkich systemów wbudowanych. Wyróżniają się
one pośród innych urządzeń tym, że komunikują się poprzez sieć
telekomunikacyjną z większymi systemami gromadzenia danych (ang. \textit{Big
    Data})~\cite{Marciniak:Odpowiedzialnosc:Bledy:IoT}. Formalnie Internet Rzeczy
(ang. \textit{Internet of Things}) można zdefiniować jako dynamiczną, globalną
infrastrukturę sieciową z samokonfiguracją i interoperacyjną
komunikacją~\cite{Zainab:IoT:Definitions:Challenges}. Dobrym przykładem
prostego urządzenia \texttt{IoT} jest inteligentny czujnik światła. Mierzy on
wartości natężenia światła w otoczeniu i udostępnia je w sieci, aby pozostałe
urządzenia mogły z takiej informacji skorzystać do automatyzacji
procesu~\cite{Marciniak:Odpowiedzialnosc:Bledy:IoT}. Dużo bardziej
zaawansowanym urządzeniem \texttt{IoT} może być samochód autonomiczny. Składa
się on z wielu mniejszych urządzeń pomiarowych oraz często central sterowania,
z którymi musi wymieniać na bieżąco informacje o ruchu
drogowym~\cite{Marciniak:Odpowiedzialnosc:Bledy:IoT}.

\section{Hardware}

Urządzenia \texttt{IoT} ze względu na wymagania wymiarowe powinny być małe i
energooszczędne. Z tego względu w projektach hobbistycznych często można
spotkać urządzenia oparte o mikrokontroler na architekturze \texttt{AVR} typu
\texttt{Arduino} czy
\texttt{ESP8266}/\texttt{ESP32}~\cite{Muc:Arduino:Raspberry}. Urządzenia te są
jednak ograniczone ze strony
oprogramowania~(rozdz.~\ref{sec:software-embedded}), co uniemożliwia
wykorzystanie takich urządzeń w przemyśle. W tym celu stosuje się bardziej
wydajne mikrokomputery oparte na architekturze \texttt{ARM}.
Najpopularniejszym wyborem staje się tutaj mikrokomputer Raspberry Pi. Jest to
komputer rozmiaru karty kredytowej, który zawiera wysoko wydajny procesor,
wyprowadzenia cyfrowe ogólnego przeznaczenia czy moduł komunikacji
bezprzewodowej \texttt{WiFi} oraz
\texttt{Bluetooth}~\cite{Quadri:IoT:surveillance:rpi}.
Prace~\cite{Quadri:IoT:surveillance:rpi,Saxena:MQTT:HomeAssistant,
    Sarthak:Raspberry:HomeAutomation:Email} przedstawiają wykorzystanie
mikrokomputera Raspberry Pi w systemach automatyki domowej. Wybór taki oparty
jest o możliwość zainstalowania na takim komputerze specjalnej wersji
\texttt{GNU/Linux}, która pozwala na prostszą implementację
oprogramowania~(rozdz.~\ref{sec:software-embedded}).

\section{Oprogramowanie}\label{sec:software-embedded}

Celem oprogramowania w systemach wbudowanych jest realizacja pewnego z góry
założonego algorytmu. Często w mniej skomplikowanych urządzeniach sprowadza się
to do wykonania cyklicznie kilkunastu operacji na pamięci czy magistralach
dostępnych w procesorze~\cite{Marchewka:Embedded:Linux}. Systemy wbudowane
oparte o pojedynczy mikrokontroler przeważnie programuje się w języku
\texttt{C} w postaci ,,bare-metal''. Nie wymaga to wykorzystania warstwy
abstrakcji sprzętowej w postaci systemu
operacyjnego~\cite{Marchewka:Embedded:Linux}. Jednak bardziej złożone systemy
czy algorytmy niejednokrotnie wykonują wiele zadań jednocześnie.
Zaimplementowanie wydajnego mechanizmu \textit{quasi-równoległości} jest często
nieefektywne lub zbyt mało wartościowe~\cite{Marchewka:Embedded:Linux}. W
takich zastosowaniach doskonale sprawdza się minimalny system operacyjny
przeznaczony dla systemów wbudowanych. Na rynku występuje wiele systemów
przeznaczonych dla systemów wbudowanych takich jak:
\begin{itemize}
    \item różne rodzaje GNU/Linux,
    \item Android,
    \item Symbian.
\end{itemize}
Ze względu na wielokrotnie większe oczekiwania takich systemów względem
mierzenia czasu, w takich zastosowaniach wykorzystuje się często modyfikacje
systemu \texttt{GNU/Linux} spełniające wymagania
RT~\cite{Vun:Embedded:Linux:RT:Modifications}.

\subsection{GNU/Linux}

System \texttt{GNU/Linux} posiada typowy dla systemów operacyjnych podział na
przestrzeń jądra i przestrzeń użytkownika. Przestrzeń jądra odpowiada za
bezpośrednią komunikację ze sprzętową pamięcią, jak i obsługę magistral.
Przestrzeń użytkownika z drugiej strony jest to miejsce, gdzie wykonywane są
wszystkie procesy implementowane przez osobę korzystającą z systemu. Podział
taki umożliwia stworzenie poziomu abstrakcji ponad warstwą sprzętową, przez co
upraszcza użytkownikowi implementację programów. Programista implementujący
programy w systemie \texttt{GNU/Linux} nie jest jednak ograniczony tylko to
przestrzeni użytkownika. System ten pozwala na implementacje komponentów jądra,
czyli modułów, które można uruchomić w jego przestrzeni. Daje to możliwość
bezpośredniej interakcji z warstwą sprzętową~\cite{Marchewka:Embedded:Linux}.

\subsection{Systemy budowy Linuksa}

System operacyjny \texttt{GNU/Linux} ze względu na otwartoźródłowość (z ang.
\textit{open-source}) pozwala na wiele modyfikacji oraz dostosowanie go do
wymagań technicznych. Jego ogólnodostępność, jak i skalowalność umożliwia
zastosowanie na wielu architekturach
mikroprocesorowych~\cite{Marchewka:Embedded:Linux}. W środowisku komercyjnym
bardzo często wykorzystuje się dedykowane obrazy systemu \texttt{GNU/Linux}
przygotowane przy pomocy specjalistycznych narzędzi kompilacji skrośnej, takich
jak \texttt{Yocto}~\cite{LinuxFoundation:YoctoProject},
\texttt{Armbian}~\cite{Armbian} czy \texttt{Buildroot}~\cite{Buildroot}. Ma to
ogromne znaczenie przy podniesieniu wydajności całego systemu oraz jego
bezpieczeństwa. Narzędzia te pozwalają na stworzenie reguł kompilacji i
konfiguracji systemu, umożliwiające konfigurowanie wersji jądra czy dodawanie
kolejnych modułów. Dodatkowo dają możliwość, by w prosty sposób modyfikować
pliki w głównym systemie plików, przez co system uruchamiany pierwszy raz może
być już w pełni skonfigurowany pod jego przeznaczenie.

\subsection{Oprogramowanie węzłów centralnych}

Systemy wbudowane, a w szczególności urządzenia \texttt{IoT} przeważnie
komunikują się z większymi systemami lub aplikacjami w celu agregacji danych czy
udostępnienia ich użytkownikowi. Dodatkowo urządzenia \texttt{IoT} często
znajdują zastosowanie w kontrolowaniu domowych urządzeń, takich jak źródła
światła, klimatyzatory czy wiatraki~\cite{Saxena:MQTT:HomeAssistant}. W celu
kontroli urządzeniami, które podłączone są do sieci, stosuje się
centralne serwery sterowania. Ich zadaniem jest implementacja logiki
automatyzacji procesu oraz wizualizacja tego użytkownikowi. Jedną z
popularniejszych aplikacji służącej jako centrum sterowania jest \texttt{Home
    Assistant}~\cite{Saxena:MQTT:HomeAssistant}. W
pracy~\cite{Saxena:MQTT:HomeAssistant} porównano go do innego systemu --
\texttt{OpenHAB}. Przedstawiono również, że oprogramowania takie dobrze
sprawdzają się jako kontrolerzy wewnątrz systemów w przypadku zadań okresowych i
zadań opartych na zdarzeniach, które dodatkowo zmniejszają interwencję człowieka
i szanse pominięcia. \texttt{Home Assistant} jest to otwartoźródłowy projekt
zaimplementowany w języku \texttt{Python}, który pozwala na integrację z
większymi systemami automatyki domowej jak \texttt{Google Home} czy
\texttt{Amazon Alexa}. Wspiera on również integrację z niestandardowymi
urządzeniami poprzez protokół \texttt{MQTT} (ang. \textit{Message Queue
    Telemetry Transport}).

\section{Systemy rozproszone}

System rozproszony to zbiór urządzeń lub programów działających wspólnie w celu
zrealizowania zdefiniowanego planu. W literaturze można znaleźć następującą
definicję ,,System rozproszony jest to zestaw niezależnych komputerów,
sprawiający na jego użytkownikach wrażenie jednego, logicznie zwartego
systemu''~\cite{Tanenbaum:Systemy:Rozproszone:Paradygmaty}. Pozwala on na
wyeliminowanie centralnych punktów awarii lub modułów, które ograniczają
wydajność. W momencie pojawienia się usterki w takim systemie pozwala on na
usunięcie niedogodności bez potrzeby ingerencji w pozostałe części struktury.
Charakteryzuje się dużymi możliwościami rozwoju poprzez rozszerzenie o
dodatkowe moduły. Dobrym przykładem systemu rozproszonego może być dzisiejszy
Internet. Znajdując się w każdym miejscu na Ziemi mamy jednakowy dostęp do
wszystkich informacji dzięki zdecentralizowaniu serwerów oraz stacji
roboczych~\cite{Rogalska:Podstawy:Topologii}.

Systemy rozproszone bardzo polegają na wewnętrznej komunikacji pomiędzy
modułami. Pojedynczy moduł zawierający jeden czy więcej czujników komunikuje
się z nimi poprzez protokoły komunikacyjne: \texttt{1-Wire}, \texttt{SPI},
\texttt{I2C}, \texttt{UART}. Wymiana danych na poziomie modułowym wymaga jednak
wyższej warstwy abstrakcji. W tym celu stosuje się protokoły modelu
\texttt{TCP/IP}.  Do najczęściej wybieranych protokołów komunikujących węzły
rozproszonych systemów \texttt{IoT} należą
\texttt{MQTT}~\cite{Saxena:MQTT:HomeAssistant}, \texttt{HTTPS},
\texttt{Modbus}~\cite{Wilczkiewicz:security:sensor:network}.

W pracy~\cite{Olszyna:Rozproszony:ZigBee} przedstawiono użyteczność technologii
ZigBee w rozproszonym systemie pomiarowym. Do wykonania pomiarów użyto tam
czujników temperatury XBee firmy MaxStream. W celu udostępnienia wyników na
stronie \texttt{WWW} napisano aplikację w języku \texttt{.NET}. Technologia
ZigBee definiuje dwa rodzaje urządzeń umieszczanych w takich systemach.
Pierwszy typ urządzeń to FFD (ang. \textit{Full Function
    Device})~\cite{Olszyna:Rozproszony:ZigBee}. Charakteryzuje się tym, że
implementuje pełną funkcjonalność, a więc można takich urządzeń użyć
samodzielnie. Najczęściej takie urządzenia można spotkać w systemach o
topologii peer-to-peer. Drugim typem urządzeń używanym w systemach
rozproszonych jest RFD (ang. \textit{Reduced Function
    Device})~\cite{Olszyna:Rozproszony:ZigBee}. Ze względu na ograniczoną
funkcjonalność urządzenia można je spotkać tylko w topologii systemu
rozproszonego wykorzystującego koordynatora -- główne urządzenie agregujące
dane lub udostępniające je użytkownikowi.

\subsection{Topologia peer-to-peer}\label{ssec:topologia-peer-to-peer}

Konfiguracja peer-to-peer opiera się na bezpośrednich połączeniach pomiędzy
urządzeniami, a każde urządzenie jest w pełni funkcjonalne i może zostać użyte
samodzielnie. Rysunek~\ref{fig:topologia-peer-peer-rozproszony} przedstawia
\begin{figure}[hbpt]
    \centering
    \includegraphics[width = \textwidth]{images/topologia-peer-peer-diagram.pdf}
    \caption{Topologia peer-to-peer w systemach rozproszonych}\label{fig:topologia-peer-peer-rozproszony}
\end{figure}
architekturę systemu rozproszonego opartego o topologię peer-to-peer.
Najczęściej, systemy wykorzystujące taką architekturę zmagają się z problemem
przesyłu dużej ilości danych. W pracy~\cite{Djellabi:P2P:Effective:Query}
przedstawiono przykładowe rozwiązanie problemu pobierania danych z systemów
rozproszonych opartych o topologię peer-to-peer. Niedogodności takie
spowodowane są aspektem przekazywania danych z jednego urządzenia do kolejnego.
Zapytanie o dane musi przejść przez tablicę mieszania, a nie istnieją efektywne
algorytmy wyszukiwania
mieszanego~\cite{Popczyk:Peer-to-Peer:zastosowania:problemy}. Z tego powodu
systemy takie są z reguły mniej efektywne niż odpowiedniki systemów
kontrolowanych. Dodatkowo systemy takie cierpią z powodu bezpieczeństwa. Nie
posiadają one centralnego punktu certyfikacji, co sprawia, że mogą być narażone
na problemy z identyfikacją~\cite{Popczyk:Peer-to-Peer:zastosowania:problemy}.

Systemy oparte o topologię peer-to-peer świetnie sprawdzają się jako systemy
wymiany plików. Przede wszystkim zastosowanie takie znajdują w dziale muzycznym
czy wideo~\cite{Popczyk:Peer-to-Peer:zastosowania:problemy}. Problemem
wykorzystania takiej architektury w systemach \texttt{IoT} może być potrzeba
implementacji w każdym z urządzeń sposobu agregacji danych. Urządzenia oparte o
system wbudowany z definicji mają odpowiadać za tylko jedno zadanie oraz być
efektywne. Implementowanie dodatkowych mechanizmów zarządzających danymi może
być nieefektywne lub powodować niepotrzebne komplikacje.

\subsection{Topologia gwiazdy}

W odróżnieniu od topologii
peer-to-peer~(rozdz.~\ref{ssec:topologia-peer-to-peer}), systemy oparte o
architekturę gwiazdy posiadają jeden główny moduł, który komunikuje się ze
światem lub udostępnia zebrane dane dalej dla
użytkownika~\cite{Rogalska:Podstawy:Topologii}.
Rysunek~\ref{fig:topologia-gwiazdy-rozproszony} przedstawia architekturę
systemu
\begin{figure}[hbpt]
    \centering
    \includegraphics[width = \textwidth]{images/topologia-gwiazdy-diagram.pdf}
    \caption{Topologia gwiazdy w systemach rozproszonych}\label{fig:topologia-gwiazdy-rozproszony}
\end{figure}
opartego o taką topologię. System rozproszony o topologii gwiazdy został
przedstawiony w pracy~\cite{Saari:Star:Linux:Network}. Celem tej pracy było
stworzenie sieci urządzeń pomiarowych wykorzystującej systemy wbudowane oparte
na systemie Linux. Dodatkowo jednym z zadań było udostępnienie tych wyników w
publicznym internecie. W tym celu posłużono się jedną z wersji topologii
gwiazdy -- master-slave. Architektura taka składa się z jednego głównego
komputera (serwera) oraz co najmniej jednego urządzenia wykonawczego
wykonującego pomiary. Wykorzystanie dokładnie takiego wariantu topologii
gwiazdy pozwala na użycie słabszych lub mniej wydajnych urządzeń końcowych, a
tylko jednego mocniejszego komputera agregującego dane. Koncepcja taka została
przedstawiona w pracy~\cite{Lipka:IoT:LoRaWAN} w celu zaoszczędzenia energii.
Użyto tam technologii LoRaWAN z pojedynczą bramką komunikującą się ze światem
zewnętrznym. Topologia wykorzystująca pojedynczego koordynatora pozwala na
kolejną dogodność -- samodzielną konfigurację zbioru urządzeń IoT, co
przedstawiono w pracy~\cite{Bogacz:Samokonfiguracja:IoT}. Przy wykorzystaniu
algorytmów uczenia maszynowego potwierdzono, że możliwe jest samodzielnie
konfigurowanie się urządzeń umieszczonych wewnątrz takiej sieci.
